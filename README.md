A simple example of installing Jenkins using docker-compose


> Need a recent version of docker-compose


In this example we create three services:

1. **jenkins-docker** based on docker:dind imae (we need it to execute Docker commands inside Jenkins nodes)
2. **jenkins-blueocean** based on the jenkinsci/blueocean image
3. **jenkins-gitlab**